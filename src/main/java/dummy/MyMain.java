package dummy;

public class MyMain {
    public static void main(String[] args) {
        System.setProperty("myprop", "edw edw");
        String hello = System.getProperty("myprop", "SOME default");
        System.out.println(hello);

        String os_name = System.getProperty("os.name");
        System.out.println(os_name);
    }
}
