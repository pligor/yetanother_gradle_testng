package api;

import api.model.InventoryItem;
import io.restassured.response.Response;

/**
 * https://app.swaggerhub.com/apis/plig/hwsample/1.0.0
 */
public class MyDummyApi extends ApiBase {
    //http://www.mocky.io
    public static final String LOTTO_ENDPOINT = "/v2/5d46ac523200004c29ae8c79";
    public static final String HELLO_ENDPOINT = "/v2/5d46abf73200004c29ae8c77";

    public static Response getHelloResponse() {
        return setup().get(HELLO_ENDPOINT);
    }

    public static String getHelloValue() {
        return setup().get(HELLO_ENDPOINT).as(
                InventoryItem.class).getHello();
    }
}