package api;

//TODO import api.model.auth.TokenView;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
//import utils.Logger;

import static io.restassured.RestAssured.given;

public class ApiBase {
    public static RequestSpecification setup() {
        return given().
                baseUri("http://www.mocky.io").contentType(ContentType.JSON);
    }
}
