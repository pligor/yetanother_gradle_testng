package gui.pageObject;


import com.codeborne.selenide.Configuration;

public class InPage extends AbstractPage<InPage> {
    public static String url() {
     return Configuration.baseUrl + "/";
    }
}
