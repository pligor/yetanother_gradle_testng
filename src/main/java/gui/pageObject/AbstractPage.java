package gui.pageObject;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import utils.Config;

public abstract class AbstractPage<T> {
    public T startUp() {

        Configuration.timeout = 5000;
        Configuration.startMaximized = false;
        Configuration.headless = false;
        Configuration.baseUrl = Config.base_url;

        System.setProperty("webdriver.chrome.driver",
                Config.isLinux() ? "./webdrivers/chromedriver_75" : ".\\webdrivers\\chromedriver.exe");

        //Logger.ACTION("Web browser configuration is finished " + "[" + this.getClass().getSimpleName() + ".class]");

        return (T) this;
    }

    public T openPage(String url) {
        System.out.println("THIS IS THE URL: " + url);
        Selenide.open(url);
        return (T) this;
    }
}
