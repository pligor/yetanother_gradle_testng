import com.codeborne.selenide.Selenide;
import gui.pageObject.InPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class IngrEnglish {

    @Test(groups = {"gui"})
    void justOpenIngrTest() {
        InPage page = new InPage();
        page.startUp().openPage(InPage.url());
    }

    @AfterMethod
    public void close() throws InterruptedException {
        Thread.sleep(2000);
        Selenide.close();
    }
}
