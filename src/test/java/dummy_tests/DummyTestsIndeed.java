package dummy_tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DummyTestsIndeed {
    @Test(groups = {"easy"})
    public void DummyTestWithEasyTag() {
        System.out.println("so easy");
        Assert.assertEquals(4, 4);
    }

    @Test(groups = {"hard"})
    public void DummyTestWithHardTag() {
        System.out.println("so hard");
        Assert.assertEquals(3, 3);

    }

    @Test(groups = {"hard", "easy"})
    public void DummyTestWithBothTags() {
        System.out.println("both here");
        Assert.assertEquals(32, 32);
    }
}
