package api_tests;

import api.MyDummyApi;
import org.testng.Assert;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.testng.annotations.Test;

public class MyDummyApiTests {
    @Test
    public void helloToTheWorldTest() {
        //Assert.assertEquals(3, 5);
        MyDummyApi.getHelloResponse().
                then().
                assertThat().statusCode(200).
                assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("testData/MyDummyApiSchema"));
    }

    @Test(groups = {"wip"})
    public void helloSwaggerTest() {
        Assert.assertEquals(MyDummyApi.getHelloValue(), "world");
    }
}
